<?php

class SearchApiDateDateQuery extends SearchApiFacetapiTerm {
  static public function getType() {
    return 'search_api_date_string';
  }

  public function extract(array $item) {
    $extra = array();
    foreach ($this->getOperators() as $operator) {
      if (strpos($item['value'], $operator) === 0) {
        $extra['date_operator'] = $operator;
        $extra['date_value'] = substr($item['value'], strlen($operator) + 1);
      }
    }
    if (empty($extra['date_operator'])) {
      $extra['date_operator'] = $this->getDefaultOperator();
      $extra['date_value'] = $item['value'];
    }
    return $extra;;
  }

  static public function getOperators($type = 'types') {
    switch ($type) {
      case 'options':
        return array(
          'before' => t('Before'),
          'during' => t('During'),
          'after' => t('After'),
        );
      default:
        return array(
          'before',
          'during',
          'after',
        );

    }


  }

  public function getDefaultOperator() {
    return 'during';
  }

  public function build() {
    $parent_build = parent::build();
    $build = array();

    // We need to duplicate all these options per date_operator.
    foreach ($this->getOperators() as $operator) {
      foreach ($parent_build as $key => $item) {
        $item['#date_value'] = $key;
        $build[$operator . '|' . $key] = $item;
      }
    }


    if ($active = $this->adapter->getActiveItems($this->facet)) {
      $item = end($active);
      $date_operator = $item['date_operator'];
    }
    else {
      $date_operator = $this->getDefaultOperator();
    }

    // Pop the date operator in a FacetAPI legal place,
    foreach (element_children($build) as $element) {
      $build[$element]['#date_operator'] = $date_operator;
    }

    return $build;
  }


  public function execute($query) {
    // Return terms for this facet.
    $this->adapter->addFacet($this->facet, $query);

    $settings = $this->adapter->getFacet($this->facet)->getSettings();
    // Adds the operator parameter.
    $operator = $settings->settings['operator'];

    // Add active facet filters.
    $active = $this->adapter->getActiveItems($this->facet);
    if (empty($active)) {
      return;
    }

    // If this facet is active we should add the filter for it.
    if ($active = $this->adapter->getActiveItems($this->facet)) {

      // Push our facet name into the Query, so we can tag it later.
      $query_options = $query->getOption('SearchApiDateDateQuery', array());
      $query_options[$this->facet['name']] = array(
        'facet field' => $this->facet['field'],
        'filter field' => $this->facet['field'] . '_filter',
      );
      $query->setOption('SearchApiDateDateQuery', $query_options);

      $item = end($active);
      $field = $this->facet['field'] . '_filter';
      $segments = explode('-', $item['date_value'], 3);
      if (count($segments) == 2) {
        // Only the month and year have been specified.
        switch ($item['date_operator']) {
          case 'before':
            $filter = '[* TO ' . $segments[0] . '-' . $segments[1] . '-01T00:00:00]';
            break;

          case 'during':
            $filter = '[' . $segments[0] . '-' . $segments[1] . '-01T00:00:00 TO ' . $segments[0] . '-' . $segments[1] . '-01T00:00:00+1MONTH-1DAY]';
            break;

          case 'after':
            $filter = '[' . $segments[0] . '-' . $segments[1] . '-01T00:00:00+1MONTH TO *]';
            break;
        }
        $this->addFacetFilter($query, $field, $filter);
      }
      elseif (count($segments) == 3) {
        // A full date has been specified.
        switch ($item['date_operator']) {
          case 'before':
            $filter = '[* TO ' . $segments[0] . '-' . $segments[1] . '-' . $segments[2] . 'T00:00:00]';
            break;

          case 'during':
            $filter = '[' . $segments[0] . '-' . $segments[1] . '-' . $segments[2] . 'T00:00:00 TO ' . $segments[0] . '-' . $segments[1] . '-' . $segments[2] . 'T00:00:00+1DAY-1SECOND]';
            break;

          case 'after':
            $filter = '[' . $segments[0] . '-' . $segments[1] . '-' . $segments[2] . 'T00:00:00+1DAY TO *]';
            break;
        }
        $this->addFacetFilter($query, $field, $filter);
      }
    }
  }


}
