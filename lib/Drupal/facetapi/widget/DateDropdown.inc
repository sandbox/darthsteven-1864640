<?php

class SearchApiDateDateDropdownWidget extends FacetapiWidgetLinks {

  public $dateInputFormat = 'Y-m';
  public $dateOutputFormat = 'F Y';

  public function execute() {
    $element = &$this->build[$this->facet['field alias']];
    $vars = $element;
    $vars['#attributes'] = $this->build['#attributes'];
    $element = drupal_get_form('search_api_date_date_select_form', $this, $vars);
  }

  public function formatDate($date) {
    $cid = implode('::', array(
      'SearchApiDateDateDropdownWidget',
      $GLOBALS['language']->language,
      $this->dateInputFormat,
      $date,
      $this->dateOutputFormat,
    ));
    if (($cache = cache_get($cid)) && !empty($cache->data)) {
      return $cache->data;
    }
    else {
      // Read the date in.
      $date = new DateObject($date, NULL, $this->dateInputFormat);
      $formatted = date_format_date($date, 'custom', $this->dateOutputFormat, $GLOBALS['language']->language);
      cache_set($cid, $formatted);
      return $formatted;
    }
  }

  public function widgetForm(&$form, &$form_state, $vars) {

    $form['#attributes'] = $vars['#attributes'];

    $default_operator = 'before';
    $default_value = '';

    $date_value_options = array();
    foreach (element_children($vars) as $child) {
      $form['#values'][$child] = $vars[$child];
      $date_value_options[$vars[$child]['#date_value']] = $this->formatDate($vars[$child]['#date_value']);
      $default_operator = $vars[$child]['#date_operator'];
      if (!empty($vars[$child]['#active'])) {
        $default_value = $vars[$child]['#date_value'];
        $form['#reset_link'] = array($vars[$child]['#path'], array('query' => $vars[$child]['#query']));
      }
    }

    $form['date_operator'] = array(
      '#type' => 'radios',
      '#title' => t('Operator'),
      '#default_value' => $default_operator,
      '#options' => $this->build['#adapter']->getFacetQuery($this->facet['field alias'])->getOperators('options'),
      '#attributes' => array('class' => array('ctools-auto-submit')),
      '#title_display' => 'invisible',
    );

    $form['date_value'] = array(
      '#type' => 'select',
      '#title' => t('Date'),
      '#default_value' => $default_value,
      '#empty_option' => t('All'),
      '#options' => $date_value_options,
      '#attributes' => array('class' => array('ctools-auto-submit')),
      '#title_display' => 'invisible',
    );

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Search'),
      '#attributes' => array('class' => array('ctools-use-ajax', 'ctools-auto-submit-click', 'js-hide')),
    );

    // Lets add autosubmit js functionality from ctools.
    $form['#attached']['js'][] = drupal_get_path('module', 'ctools') . '/js/auto-submit.js';

  }

  public function widgetFormValidate(&$form, &$form_state) {

  }

  public function widgetFormSubmit(&$form, &$form_state) {
    $values = $form_state['values'];
    if (!empty($values['date_operator']) && !empty($values['date_value'])) {
      // Find the correct value and redirect.
      $key = $values['date_operator'] . '|' . $values['date_value'];
      if (isset($form['#values'][$key])) {
        $form_state['redirect'] = array($form['#values'][$key]['#path'], array('query' => $form['#values'][$key]['#query']));
      }
    }
    elseif (!empty($form['#reset_link'])) {
      $form_state['redirect'] = $form['#reset_link'];
    }
  }

}