<?php

class SearchAPIDateAggregator extends SearchApiAbstractAlterCallback {

  public function configurationForm() {
    $form['#attached']['css'][] = drupal_get_path('module', 'search_api') . '/search_api.admin.css';

    $fields = $this->index->getFields(FALSE);
    $field_options = array();
    foreach ($fields as $name => $field) {
      if ($field['type'] == 'date') {
        $field_options[$name] = $field['name'];
      }
    }
    $additional = empty($this->options['fields']) ? array() : $this->options['fields'];

    $types = $this->getTypes();
    $type_descriptions = $this->getTypes('description');
    $tmp = array();
    foreach ($types as $type => $name) {
      $tmp[$type] = array(
        '#type' => 'item',
        '#description' => $type_descriptions[$type],
      );
    }
    $type_descriptions = $tmp;

    $form['#id'] = 'edit-callbacks-search-api-alter-add-date-aggregation-settings';
    $form['description'] = array(
      '#markup' => t('<p>This data alteration lets you define additional date fields that will be added to this index. ' .
        'Each of these new fields will be an aggregation of one or more existing date fields.</p>' .
        '<p>To add a new aggregated date field, click the "Add new field" button and then fill out the form.</p>' .
        '<p>To remove a previously defined field, click the "Remove field" button.</p>' .
        '<p>You can also change the names or contained fields of existing aggregated date fields.</p>'),
    );
    $form['fields']['#prefix'] = '<div id="search-api-alter-add-date-aggregation-field-settings">';
    $form['fields']['#suffix'] = '</div>';
    if (isset($this->changes)) {
      $form['fields']['#prefix'] .= '<div class="messages warning">All changes in the form will not be saved until the <em>Save configuration</em> button at the form bottom is clicked.</div>';
    }
    foreach ($additional as $name => $field) {
      $form['fields'][$name] = array(
        '#type' => 'fieldset',
        '#title' => $field['name'] ? $field['name'] : t('New field'),
        '#collapsible' => TRUE,
        '#collapsed' => (boolean) $field['name'],
      );
      $form['fields'][$name]['name'] = array(
        '#type' => 'textfield',
        '#title' => t('New field name'),
        '#default_value' => $field['name'],
        '#required' => TRUE,
      );
      $form['fields'][$name]['type'] = array(
        '#type' => 'select',
        '#title' => t('Aggregation type'),
        '#options' => $types,
        '#default_value' => $field['type'],
        '#required' => TRUE,
      );
      $form['fields'][$name]['format'] = array(
        '#type' => 'textfield',
        '#title' => t('Date format'),
        '#default_value' => $field['format'],
        '#required' => TRUE,
      );
      $form['fields'][$name]['type_descriptions'] = $type_descriptions;
      foreach (array_keys($types) as $type) {
        $form['fields'][$name]['type_descriptions'][$type]['#states']['visible'][':input[name="callbacks[search_api_date_aggregator][settings][fields][' . $name . '][type]"]']['value'] = $type;
      }
      $form['fields'][$name]['fields'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Contained fields'),
        '#options' => $field_options,
        '#default_value' => drupal_map_assoc($field['fields']),
        '#attributes' => array('class' => array('search-api-alter-add-date-aggregation-fields')),
        '#required' => TRUE,
      );
      $form['fields'][$name]['weights'] = array(
        '#type' => 'fieldset',
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#title' => t('Order fields'),
      );
      foreach ($field_options as $field_name => $field_display_name) {
        $form['fields'][$name]['weights'][$field_name] = array(
          '#type' => 'weight',
          '#title' => t('Weight for @field_display_name', array('@field_display_name' => $field_display_name)),
          '#default_value' => isset($field['weights'][$field_name]) ? $field['weights'][$field_name] : 0,
          '#required' => TRUE,
          '#states' => array(
            'visible' => array(
              ':input[name="callbacks[search_api_date_aggregator][settings][fields][' . $name . '][fields][' . $field_name . ']"]' => array('checked' => TRUE),
            ),
          ),
        );
      }
      $form['fields'][$name]['actions'] = array(
        '#type' => 'actions',
        'remove' => array(
          '#type' => 'submit',
          '#value' => t('Remove field'),
          '#submit' => array('_search_api_date_add_aggregation_field_submit'),
          '#limit_validation_errors' => array(),
          '#name' => 'search_api_add_date_aggregation_remove_' . $name,
          '#ajax' => array(
            'callback' => '_search_api_date_add_aggregation_field_ajax',
            'wrapper' => 'search-api-alter-add-date-aggregation-field-settings',
          ),
        ),
      );
    }
    $form['actions']['#type'] = 'actions';
    $form['actions']['add_field'] = array(
      '#type' => 'submit',
      '#value' => t('Add new date field'),
      '#submit' => array('_search_api_date_add_aggregation_field_submit'),
      '#limit_validation_errors' => array(),
      '#ajax' => array(
        'callback' => '_search_api_date_add_aggregation_field_ajax',
        'wrapper' => 'search-api-alter-add-date-aggregation-field-settings',
      ),
    );
    return $form;
  }

  public function configurationFormValidate(array $form, array &$values, array &$form_state) {
    unset($values['actions']);
    if (empty($values['fields'])) {
      return;
    }
    foreach ($values['fields'] as $name => $field) {
      $fields = $values['fields'][$name]['fields'] = array_values(array_filter($field['fields']));
      unset($values['fields'][$name]['actions']);
      if ($field['name'] && !$fields) {
        form_error($form['fields'][$name]['fields'], t('You have to select at least one field to aggregate. If you want to remove an aggregated field, please delete its name.'));
      }
    }
  }

  public function configurationFormSubmit(array $form, array &$values, array &$form_state) {
    if (empty($values['fields'])) {
      return array();
    }
    $index_fields = $this->index->getFields(FALSE);
    foreach ($values['fields'] as $name => $field) {
      if (!$field['name']) {
        unset($values['fields'][$name]);
      }
      else {
        $values['fields'][$name]['description'] = $this->fieldDescription($field, $index_fields);
      }
    }
    $this->options = $values;
    return $values;
  }

  public function alterItems(array &$items) {
    if (!$items) {
      return;
    }
    if (isset($this->options['fields'])) {
      $types = $this->getTypes('type');
      foreach ($items as $item) {
        $wrapper = $this->index->entityWrapper($item);
        foreach ($this->options['fields'] as $name => $field) {
          if ($field['name']) {
            $required_fields = array();
            $weighted_fields = $this->orderFields($field['fields'], $field['weights']);
            foreach ($weighted_fields as $f) {
              if (!isset($required_fields[$f])) {
                $required_fields[$f]['type'] = $types[$field['type']]['filter'];
              }
            }
            $fields = search_api_extract_fields($wrapper, $required_fields);
            $values = array();
            foreach ($fields as $f) {
              if (isset($f['value'])) {
                $values[] = $f['value'];
              }
            }
            $values = $this->flattenArray($values);

            $this->reductionType = $field['type'];
            $this->reductionFormat = $field['format'];

            switch ($field['type']) {
              case 'combine':
                $item->$name = array_map(array($this, 'reduce'), $values);
                $item->{$name . '_filter'} = $values;
                break;

              case 'first':
                $mapped = array_map(array($this, 'reduce'), $values);
                $item->$name = array_shift($mapped);
                $item->{$name . '_filter'} = array_shift($values);
                break;
            }
          }
        }
      }
    }
  }

  /**
   * Helper method for ordering the fields that this aggregator aggregates.
   */
  public function orderFields($fields, $weights) {
    $weighted = array();
    foreach ($fields as $key => $field_name) {
      $weighted[$field_name] = isset($weights[$field_name]) ? (int) $weights[$field_name] : 0;
    }
    asort($weighted, SORT_NUMERIC);
    return array_keys($weighted);
  }

  /**
   * Helper method for reducing an array to a single value.
   */
  public function reduce($a) {
    return date($this->reductionFormat, $a);
  }

  /**
   * Helper method for flattening a multi-dimensional array.
   */
  protected function flattenArray(array $data) {
    $ret = array();
    foreach ($data as $item) {
      if (!isset($item)) {
        continue;
      }
      if (is_scalar($item)) {
        $ret[] = $item;
      }
      else {
        $ret = array_merge($ret, $this->flattenArray($item));
      }
    }
    return $ret;
  }

  public function propertyInfo() {
    $types = $this->getTypes('type');
    $ret = array();
    if (isset($this->options['fields'])) {
      foreach ($this->options['fields'] as $name => $field) {
        $ret[$name] = array(
          'label' => $field['name'],
          'description' => empty($field['description']) ? '' : $field['description'],
          'type' => $types[$field['type']]['facet'],
        );
        $ret[$name . '_filter'] = array(
          'label' => $field['name'] . ' Filter',
          'description' => empty($field['description']) ? '' : $field['description'],
          'type' => $types[$field['type']]['filter'],
        );
      }
    }
    return $ret;
  }

  /**
   * Helper method for creating a field description.
   */
  protected function fieldDescription(array $field, array $index_fields) {
    $fields = array();
    foreach ($field['fields'] as $f) {
      $fields[] = isset($index_fields[$f]) ? $index_fields[$f]['name'] : $f;
    }
    $type = $this->getTypes();
    $type = $type[$field['type']];
    return t('A @type aggregation of the following fields: @fields.', array('@type' => $type, '@fields' => implode(', ', $fields)));
  }

  /**
   * Helper method for getting all available aggregation types.
   *
   * @param $info (optional)
   *   One of "name", "type" or "description", to indicate what values should be
   *   returned for the types. Defaults to "name".
   *
   */
  protected function getTypes($info = 'name') {
    switch ($info) {
      case 'name':
        return array(
          'combine' => t('Combine'),
          'first' => t('First valid value'),
        );
      case 'type':
        return array(
          'combine' => array(
            'facet' => 'list<string>',
            'filter' => 'list<date>',
          ),
          'first' => array(
            'facet' => 'string',
            'filter' => 'date',
          ),
        );
      case 'description':
        return array(
          'combine' => t('Combine the date fields into one.'),
          'first' => t('Take the value from the first field that has a value set.'),
        );
    }
  }

  /**
   * Submit helper callback for buttons in the callback's configuration form.
   */
  public function formButtonSubmit(array $form, array &$form_state) {
    $button_name = $form_state['triggering_element']['#name'];
    if ($button_name == 'op') {
      for ($i = 1; isset($this->options['fields']['search_api_date_aggregator' . $i]); ++$i) {
      }
      $this->options['fields']['search_api_date_aggregator' . $i] = array(
        'name' => '',
        'type' => 'combine',
        'format' => 'Y-m',
        'fields' => array(),
        'weights' => array(),
      );
    }
    else {
      $field = substr($button_name, 39);
      unset($this->options['fields'][$field]);
    }
    $form_state['rebuild'] = TRUE;
    $this->changes = TRUE;
  }

}


/**
 * Submit function for buttons in the callback's configuration form.
 */
function _search_api_date_add_aggregation_field_submit(array $form, array &$form_state) {
  $form_state['callbacks']['search_api_date_aggregator']->formButtonSubmit($form, $form_state);
}

/**
 * AJAX submit function for buttons in the callback's configuration form.
 */
function _search_api_date_add_aggregation_field_ajax(array $form, array &$form_state) {
  return $form['callbacks']['settings']['search_api_date_aggregator']['fields'];
}